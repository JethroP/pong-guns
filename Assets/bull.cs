﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bull : MonoBehaviour
{
    Rigidbody rb;
    // Start is called before the first frame update
    // Update is called once per frame
    void Update()
    {

    }
    void OnCollisionEnter(Collision hit)
    {
        if (hit.gameObject.name == "top")
        {
            float speed = 0f;
            if (rb.velocity.x > 0f)
                speed = 6f;
            if (rb.velocity.x < 0f)
                speed = -6f;

            rb.velocity = new Vector3(speed, -6f, 0f);
        }
        if (hit.gameObject.name == "bottom")
        {
            float speed = 0f;
            if (rb.velocity.x > 0f)
                speed = 6f;
            if (rb.velocity.x < 0f)
                speed = -6f;
            rb.velocity = new Vector3(speed, 6f, 0f);

        }
        if (hit.gameObject.name == "bat1")
        {
            float speed = 0f;
            if (rb.velocity.x > 0f)
                speed = 6f;
            if (rb.velocity.x < 0f)
                speed = -6f;
            rb.velocity = new Vector3(speed, speed, 0f);

        }
        if (hit.gameObject.name == "bat2")
        {
            float speed = 0f;
            if (rb.velocity.x > 0f)
                speed = 6f;
            if (rb.velocity.x < 0f)
                speed = -6f;
            rb.velocity = new Vector3(speed, speed, 0f);

        }
    }
}
