﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fire1 : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform firepoint;
    public GameObject bulletprefab;
    private void Update()
    {
       
        if (Input.GetKey(KeyCode.Space))
        {
            Shoot();
        }

    }
    void Shoot()
    {
        Instantiate(bulletprefab, firepoint.position, firepoint.rotation);
    }
}
