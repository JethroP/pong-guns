﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerinput : MonoBehaviour
{
    public GameObject leftbat;
    public GameObject rightbat;
    public float speed = 8f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 moveDir = Vector3.zero;
        moveDir.x = Input.GetAxis("Horizontal"); 
        moveDir.z = Input.GetAxis("Vertical");
        if (moveDir.x !=null)
        {
            leftbat.transform.position += moveDir * speed * Time.deltaTime;
        }
        rightbat.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
        if (Input.GetKey(KeyCode.UpArrow))
        {
            rightbat.GetComponent<Rigidbody>().velocity = new Vector3(0f, 8f, 0f);
            //Move the bat up
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            rightbat.GetComponent<Rigidbody>().velocity = new Vector3(0f, -8f, 0f);
            //Move the bat down
        }
    }
}
