﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour
{
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        //Chooses a direction and runs
        rb = GetComponent<Rigidbody>();
        int xDir = Random.Range(0, 2);
        int yDir = Random.Range(0, 3);
        Vector3 launchdir = new Vector3();
        if (xDir == 0)
        {
            launchdir.x = -6f;
        }
        if (xDir == 1)
        {
            launchdir.x = 6f;
        }
        if (yDir == 0)
        {
            launchdir.y = -6f;
        }
        if (yDir == 1)
        {
            launchdir.y = 6f;
        }
        if (yDir == 2)
        {
            launchdir.y = 0f;
        }
        rb.velocity = launchdir;


    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnCollisionEnter(Collision hit)
    {
        if (hit.gameObject.name == "top")
        {
            float speed = 0f;
            if (rb.velocity.x > 0f)
                speed = 6f;
            if (rb.velocity.x < 0f)
                speed =-6f;

            rb.velocity = new Vector3(speed,-6f, 0f);
        }
        if (hit.gameObject.name == "side ball")
        {
            float speed = 0f;
            if (rb.velocity.x > 0f)
                speed = 6f;
            if (rb.velocity.x < 0f)
                speed = -6f;
            rb.velocity = new Vector3(speed, 6f, 0f);

        }
        if (hit.gameObject.name == "bat1")
        {
            float speed = 0f;
            if (rb.velocity.x > 0f)
                speed = 6f;
            if (rb.velocity.x < 0f)
                speed = -6f;
            rb.velocity = new Vector3(speed, speed, 0f);

        }
        if (hit.gameObject.name == "bat2")
        {
            float speed = 0f;
            if (rb.velocity.x > 0f)
                speed = 6f;
            if (rb.velocity.x < 0f)
                speed = -6f;
            rb.velocity = new Vector3(speed, speed, 0f);

        }
        if (hit.gameObject.name == "bullet")
        {
            float speed = 0f;
            if (rb.velocity.x > 0f)
                speed = 6f;
            if (rb.velocity.x < 0f)
                speed = -6f;
            rb.velocity = new Vector3(speed, speed, 0f);

        }
    }
}
