﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fire : MonoBehaviour
{
    public Transform firepoint;
    public GameObject bulletprefab;
    private void Update()
    {
        if (this.transform.name == "bat1")
        {
            if (Input.GetButtonDown("Fire2"))
            {
                Shoot();
            }
        }
        if(this.transform.name == "bat2")
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Shoot();
            }
        }
    }
    void Shoot()
    {
        Instantiate(bulletprefab, firepoint.position, firepoint.rotation);
    }
}

