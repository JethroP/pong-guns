﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class warp : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform warptarget;
    private void OnTriggerEnter(Collider other)
    {
        other.gameObject.transform.position = warptarget.position;
    }
}
